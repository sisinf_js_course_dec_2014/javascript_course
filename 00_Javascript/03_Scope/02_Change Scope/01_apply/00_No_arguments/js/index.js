(function () {
    var obj = {};
    function setTest(){
        this.test = 'test';
    }
    document.writeln('Before executing setGreeting');
    document.writeln('<br>');
    document.writeln('obj: ' + JSON.stringify(obj));
    document.writeln('<br>');
    document.writeln('window.test: ' + window.test);
    document.writeln('<br><br>');

    document.writeln('Execute setTest without change scope (Window)');
    setTest();
    document.writeln('<br>');
    document.writeln('obj: ' + JSON.stringify(obj));
    document.writeln('<br>');
    document.writeln('window.test: ' + window.test);
    document.writeln('<br><br>');

    document.writeln('Execute setTest using obj as scope.');
    setTest.apply(obj);
    document.writeln('<br>');
    document.writeln('obj: ' + JSON.stringify(obj));
    document.writeln('<br>');
    document.writeln('window.test: ' + window.test);
    document.writeln('<br>');

}());