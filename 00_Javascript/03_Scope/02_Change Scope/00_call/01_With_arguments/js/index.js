(function () {
    var obj = {};
    function setGreeting(name, surname){
        this.greeting = 'Hello,  ' + surname + ',' + name;
    }
    document.writeln('Before executing setGreeting');
    document.writeln('<br>');
    document.writeln('obj: ' + JSON.stringify(obj));
    document.writeln('<br>');
    document.writeln('window.test: ' + window.greeting);
    document.writeln('<br><br>');

    document.writeln('Execute setGreeting without change scope (Window)');
    setGreeting('Tomas', 'Corral');
    document.writeln('<br>');
    document.writeln('obj: ' + JSON.stringify(obj));
    document.writeln('<br>');
    document.writeln('window.test: ' + window.greeting);
    document.writeln('<br><br>');

    document.writeln('Execute setGreeting using obj as scope.');
    setGreeting.call(obj, 'Tomas', 'Corral');
    document.writeln('<br>');
    document.writeln('obj: ' + JSON.stringify(obj));
    document.writeln('<br>');
    document.writeln('window.test: ' + window.greeting);
    document.writeln('<br>');

}());