var obj = { key: 'value' };

function modify( obj ){
    obj['otherKey'] = 'other value';
    document.writeln(JSON.stringify(obj));
}

document.writeln('Modify execution: ... ');
modify(obj);

document.writeln('<br>Object after execute modify: ... ');
document.writeln(JSON.stringify(obj));