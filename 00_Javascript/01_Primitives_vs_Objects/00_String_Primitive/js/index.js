var primitiveValueString = 'Hello';

function modify( argString ){
    argString += ' World!';
    document.writeln(argString);
}

document.writeln('Modify execution: ... ');
modify(primitiveValueString);

document.writeln('<br>Primitive value after execute modify: ... ');
document.writeln(primitiveValueString);