var array = [1,2];

function modify( arr ){
    arr.push(3);
    document.writeln(arr);
}

document.writeln('Modify execution: ... ');
modify(array);

document.writeln('<br>Array after execute modify: ... ');
document.writeln(array);