var myVar = 10; // When this code is evaluated you will have available window.myVar.

function setTest(){     //  When the code is evaluated you will have available window.setTest
    this.test = 'test';
}

setTest();  // Executing this line you will have available window.test.

(function () {  // Executing this IIFE you will have available window.b and window.c;
    var a = 10
        b = 20,
        c = 30;
}());

function myFunc() { //  When the code is evaluated you will have available window.myFunc
    d = 40;
}

myFunc();   // Executing this line you will have available window.d.