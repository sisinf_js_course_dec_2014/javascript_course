(function () {
    var myVar = 10; // Using var inside a function will make the variable to remain private outside of the function.
    function setTest(){     //  Function declaration will remain private outside of the function
        this.test = 'test';
    }

    //setTest();  // This function should not be called because it will pollute globals.


    (function () {  // Remember to add the comma before each new declaration or initialization of variables
        var a = 10,
            b = 20,
            c = 30;
    }());

    function myFunc() {
        var d = 40; // Never forget to add the var statement when we declare variables.
    }

    myFunc();

    window.myFunc = myFunc; // This line of code will add the myFunc to window but exposing it explicitly.
}());





