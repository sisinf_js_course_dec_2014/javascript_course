(function () {
    'use strict';

    function timedProcessArray(nItems, process, callback){

        setTimeout(function recursive(){
            var start = +new Date();

            do{
                process(--nItems);
            }while(nItems > 0 && (+new Date() - start < 50));

            if(nItems > 0){
                setTimeout(recursive, 25);
            }else{
                callback();
            }
        }, 25);

    }

    var arr = [34, 203, 3, 746, 200, 984, 198, 764, 9, 10, 200, 302, 293, 294, 853, 392, 222, 321, 11, 1, 239, 223, 948, 992, 2, 33, 45, 220];
    var all = [];
    var executionsNode = document.getElementById('executions');
    var expentTimeNode = document.getElementById('expentTime');

    function bubbleSort(arr) {
        var a = arr.concat();
        var swapped;
        do {
            swapped = false;
            for (var i = 0; i < a.length - 1; i++) {
                if (a[i] > a[i + 1]) {
                    var temp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = temp;
                    swapped = true;
                }
            }
        } while (swapped);
        console.log(a);
        return a;
    }

    function doBubbleSort(){
        var nStart = window.performance.now(),
            nEnd;
        timedProcessArray(10000, function (index){
            all = all.concat(bubbleSort(arr));
            executionsNode.innerHTML = index;
        }, function (){
            nEnd = window.performance.now() - nStart;
            expentTimeNode.innerHTML = nEnd + ' ms';
        });

    }

    document.getElementById('add').addEventListener('click', function () {
        doBubbleSort();
    }, false);
}());